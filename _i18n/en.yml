---
title: Mural - Berta & Marielle
description: Mural en memoria de Berta Cáceres y Marielle Franco

index:
  title: You have the bullet, we have the word
  description: Mural in memory of Berta Cáceres & Marielle Franco

  where: "<h4>Here you can visit the mural:</h4><p>Housing project M29 at Malmöer Str. 29, 10439 Berlin-Prenzlauer Berg</p>"

about:
  title: About us
  description: Who is behind this project?

  one:
    "<header class='major'>
      <h1>About us</h1>
    </header>
    <p>
      The mural is located on the façade of the <a href='https://hausprojekt-m29.org/' target='_blank'>housing project M29</a> at Malmöer Str. 29 in Berlin-Prenzlauer Berg. Thanks again at this point for providing the house wall.
    </p>
    <p>
      The project was initiated and implemented by CADEHO (human rights collective for Honduras) and collectively orangotango (collective for critical education and creative protest). The mural was designed and painted by Fonso, La Negra and Soma, three Colombian artists.
    </p>
    <p>
      Many thanks also to the <a href='https://www.luisenstadteg.de/' target='_blank'>Luisenstadt EG</a> for the color support and to the <a href='https://www.fdcl.org/' target='_blank'>Forschungs- und Dokumentationszentrum Chile-Lateinamerika e.V.</a> (FDCL) for organizational and administrative support.
    </p>
    <p>
      <b>Idea and implementation</b>"

  two:
    "<b>This project was made possible by the financial support of:</b>"

aktiv_werden:
  title: Become active
  description: How can you participate?

  one:
    "<header class='major'>
      <h4>The murders of Berta and Marielle have caused a sensation, anger and sadness worldwide and led to numerous protests and solidarity actions. Along with all these people and social movements, we demand:</h4>
    </header>
    <br>
    <ul>
      <li>
        Justice for Berta, Marielle and all the other murdered human rights and environmental activists
    	</li>
      <li>
        The complete investigation of the murders as well as the prosecution of all participants
    	</li>
      <li>
        A fundamental change in the business policies of companies and corporations that generate their profits at the expense of environmental and human rights
    	</li>
    	<li>
        The establishment of independent control institutions that control and ensure that businesses' businesses do not benefit from human rights violations and environmental degradation
    	</li>
      <li>
        The stop of arms exports
    	</li>
      <li>
        The protection and solidarity with all the people who work for social justice in Latin America and elsewhere, risking their lives every day
    	</li>
    </ul>"

  two:
    "<h4>There are many ways to get active yourself, for example:</h4>
    <div class='box'>
    	<b><a href='https://copinh.org/' target='_blank'>COPINH</a></b> is constantly campaigning for the conviction of the murderers of Berta Cáceres. They regularly organize campaigns and solidarity actions that can also be supported from Germany.
    </div>

    <div class='box'>
    	The <b><a href='https://lieferkettengesetz.de/' target='_blank'>supply chain law initiative</a></b> advocates a law requiring companies to respect human rights and environmental standards throughout the supply chain. Further information and possibilities of payment.
    </div>

    <div class='box'>
    	<b>Amnesty international</b> is using online campaigns, letters and petitions to demand justice for the killings of <a href='https://www.amnesty.de/mitmachen/petition/gerechtigkeit-fuer-marielle-franco' target='_blank'>Marielle Franco</a> and <a href='https://www.amnesty.de/mitmachen/brief-gegen-das-vergessen/berta-caceres-copinh' target='_blank'>Berta Cáceres</a> in order to put pressure on authorities and local politicians.
    </div>

    <div class='box'>
    	The campaign <b><a href='https://www.stoppt-waffenexporte.de/' target='_blank'>'Stop arms export!'</a></b> calls for a general ban on exports of weapons, war equipment and ammunition.
    </div>

    <div class='box'>
      The <b><a href='https://rheinmetallentwaffnen.noblogs.org/' target='_blank'>Rheinmetall campaign</a></b> disarms itself against arms exports and war policy.
    </div>

    <div class='box'>
    	Organize and / or join existing groups, paint walls and organize protest and solidarity actions!
    </div>"

hintergrund:
  title: Background
  description: Berta Cáceres, Marielle Franco and the situation of human rights activists in Latin America

  one:
    "<p>
      Latin America is considered the most dangerous region for activists working for human and environmental rights. Every year, hundreds of people are criminalized, persecuted or murdered for engaging in land rights, environmental protection or the rights of minorities. In rural areas, conflicts take place where companies mine raw materials, build dams or clear forests. German companies and banks are also frequently involved. In Latin American cities, engaging in police violence and racism, as well as women's and minority rights, is often equally life threatening.
    </p>
    <p>
      Yet, many people put up with this risk, resisting the oppression and exploitation of common goods, and are committed to social justice and their right to self-determination. Often it is women who are at the front and risk their lives.
    </p>
    <p>
      Even though the murders, as the cases of Berta and Marielle show, silence individuals for ever, the resistant ideas and actions continue to live. Berta Cáceres aptly expressed this during her lifetime with her famous sentence: \"You have the ball, I say the ball dies when it detonates, the word lives if you pass it on.\"
    </p>
    <p>
    	Additional information:
    	<br>
    	<a href='https://www.globalwitness.org/en/' target='_blank'>www.globalwitness.org</a>
    	<br>
    	<a href='https://www.peacebrigades.org/' target='_blank'>www.peacebrigades.org</a>
    </p>"

  two:
    "<p>
      Berta Cáceres was a well-known environmental and human rights activist working with the indigenous organization COPINH against illegal mining and hydropower projects on the traditional land of the indigenous Lenca. Their resistance to the Agua Zarca hydroelectric power plant, which ia. was financed by European development banks and was to be supplied with turbines by the German companies Voith and Siemens. In 2016, she was murdered after years of threats and attacks. Their killers were commissioned by the operator of the hydroelectric power plant.
    </p>
    <p>
    	Additional Information:
    	<br>
    	<a href='https://hondurasdelegation.blogspot.com/search/label/ProzessBC' target='_blank'>Up-to-date information on Honduras and the case (in german)</a>
    	<br>
    	<a href='https://gaipe.net/wp-content/uploads/2017/10/GAIPE-Report-English.pdf' target='_blank'>Investigation report on the murder of Berta Cáceres by an independent expert commission</a>
    </p>"

  three:
    "<p>
  		Marielle Franco was a human rights defender and councilor in Rio de Janeiro. A black, lesbian and single mother, she campaigned against racism and police violence in the Brazilian metropolis and campaigned for the rights of women, LGBTIQ and Favela residents. In 2018, she and her driver Anderson Gomes were murdered by militias carrying a weapon from the German defense company Heckler & Koch, which had originally been delivered to police special forces.
  	</p>
  	<p>
  		Additional Information:
  		<br>
  		<a href='https://www.fr.de/politik/marielle-franco-gesicht-widerstands-brasilien-11777715.html' target='_blank'>Article about Marielle, her murder and her effect beyond death (in german)</a>
  		<br>
  		<a href='https://www.amnesty.de/informieren/aktuell/brasilien-ein-jahr-nach-mord-marielle-franco-behoerden-muessen-verantwortliche' target='_blank'>Statement by Amnesty International (in german)</a>
  	</p>"

  four:
    "<h3>
    	Shift to the right, militarization, violence and corruption
    </h3>
    <p>
    	The murders of social activists, journalists, and lawyers are reaching record highs in Latin America and even exceeding the numbers of war zones. The increasing shift to the right in recent years further exacerbates these developments. In addition, many countries in the region have close links between state security agencies, local politicians, businesses, and paramilitary structures and criminal organizations. The latter often engage in \"dirty work\" for politics and business in this context. But it is also the state itself that is repressing social movements and human rights defenders.
    </p>
    <p>
      These interconnections also play a role in the assassination of Berta Cáceres and Marielle Franco: in the case of Berta, several contract killers and middlemen have since been convicted, including former employees of the operator of the hydroelectric power plant (such as the ex-security chief of the company) and a Major of the Honduran Army. The suspects arrested one year after the murder of Marielle are two former Brazilian military police, with the alleged gunman living in the same housing complex as Brazil's current right-wing president Jair Bolsonaro.
    </p>"

  five:
    "<div class='12u'>
    	<h3>
    		German companies
    	</h3>
    	<p>
        German companies are active worldwide - they supply the machinery for mining projects, pesticides for the fields of agriculture and weapons (supposedly not in crisis and war zones). Again and again, human rights violations are documented in the environment of their activities.
    	</p>
    	<p>
        The turbines for the Agua Zarca hydroelectric power plant project should be supplied by VoithHydro, a joint venture between German companies Voith and Siemens. Although repeatedly referred to human rights abuses surrounding the project, they continued months after the murder of Berta Cáceres on the supply contract with the operating company. The weapon used to murder Marielle Franco was a product of the German weapon manufacturer Heckler & Koch, which had been delivered to special units of the Brazilian police. Despite German arms export control it came into the hands of militia.
    	</p>
    </div>
    <p>
    	Additional Information:
    	<br>
    	<a href='https://www.kritischeaktionaere.de/' target='_blank'>Umbrella Association of Critical Shareholders (in german)</a>
    	<br>
    	<a href='https://www.gegenstroemung.org/web/wp-content/uploads/2017/03/STUDIE_STAUDA%cc%88MME_online.pdf' target='_blank'>Study on European companies and banks in the hydropower business (in german)</a>
    	<br>
    	<a href='https://www.waffenexporte.org/' target='_blank'>Background to German arms exports (in german)</a>

    	<div class='6u 12u(small)'>
    		<img src='../assets/images/hintergrund/kugel.JPG' alt=''/>
    	</div>
    </p>"

wandbild:
  title: The mural
  description: Idea and creation process

  one: "<p>
      Using the example of Berta Cáceres and Marielle Franco, two resistant women, the mural at Malmöer Str. 29 in Berlin addresses social struggles in Latin America and recalls the work and murder of the two activists. In the form of a collage, problems such as police violence, the overexploitation of nature, the human rights responsibility of German companies, but especially the resistance against it are made visible.
		</p>
		<p>
      The mural design is in the tradition of the Mexican Muralismo: Through murals in public space sensitized to social ills and stimulated the concerns and demands of marginalized social groups visibility.
		</p>
		<p>
      The mural is on the initiative of CADEHO (human rights collective Honduras) and collectively orangotango (collective for critical education and creative protest) in collaboration with the artists of the Colombian m.a.l. crew emerged.
		</p>
		<p>
      The mural shows solidarity with the struggles of Berta, Marielle and other activists who had to pay for their commitment to a better world with their lives; Since German actors also played a role in both cases, we also want to address the responsibility of German actors for human rights crimes.
		</p>"
